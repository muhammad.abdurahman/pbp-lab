from django import forms
from django.db.models import fields
from django.forms.widgets import TextInput

from lab_2.models import Note


class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        # fields = ["noteTo", "noteFrom", "noteTitle", "noteMessage"]
        fields = '__all__'

    to = forms.CharField(widget=TextInput(
        attrs={'placeholder': 'Notes to...'}))

    From = forms.CharField(widget=TextInput(
        attrs={'placeholder': 'Notes from...'}))

    title = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': 'ex: Important Notes!!!'}))
    
    message = forms.CharField(widget=forms.Textarea(
        attrs={'placeholder': 'There are some messages here'}))
