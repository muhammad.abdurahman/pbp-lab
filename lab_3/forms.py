from django import forms
from django.forms.widgets import TextInput

from lab_1.models import Friend


class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ["name", "npm", "birth_date"]

    name = forms.CharField(widget=TextInput(
        attrs={'placeholder': 'Nama Baru'}))

    npm = forms.CharField(widget=TextInput(
        attrs={'placeholder': 'ex: 2006597241'}))

    birth_date = forms.DateField(widget=forms.DateInput(
        attrs={'placeholder': 'ex: 09/06/2002'}))
