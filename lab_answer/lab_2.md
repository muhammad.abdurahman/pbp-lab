1. Apakah perbedaan antara JSON dan XML?

a. JSON merupakan singkatan dari JavaScript Object Notation dan XML merupakan singkatan dari Extensible Markup Language
b. JSON dibuat berdasarkan JavaScript dan XML berasal dari SGML
c. JSON dibuat untuk merepresentasikan objek dan XML dibuat untuk merepresentasikan struktur data
d. JSON dapat menggunakan array dan XML tidak dapat menggunakan Array
e. JSON tidak menggunakan tag dan XML menggunakan tag
f. XML lebih aman dari JSON
g. Struktur data JSON berupa map dan struktur data XML berupa tree
h. JSON lebih cepat dari XML
i. JSON tidak mendukung namespace, metadata, dan komentar dan XML mendukung namespace, metadata, dan komentar


2. Apakah perbedaan antara HTML dan XML?

a. HTML tidak case sensitive dan XML case sensitive
b. HTML tag sudah disediakan dan XML tag dibuat sesuai kebutuhan programmer
c. Tidak semua HTML tag memiliki closing tag dan semua XML tag membutuhkan closing tag
d. HTML dibuat untuk menampilkan data dan XML dibuat untuk mentransfer data
e. HTML bersifat statis dan XML bersifat dinamis